# Random Forest Regression

## Video for explanation
You can see a good video about this topic on YouTuBe by this link -> https://www.youtube.com/watch?v=D_2LkhMJcfY

## What is Random Forest?
> Random Forest is a flexible, easy to use machine learning algorithm that produces, even without hyper-parameter tuning, a great result most of the time. It is also one of the most used algorithms, because it’s simplicity and the fact that it can be used for both classification and regression tasks. In this post, you are going to learn, how the random forest algorithm works and several other important things about it [1].

## How it Works?

> Random Forest is a supervised learning algorithm. Like you can already see from it’s name, it creates a forest and makes it somehow random. The „forest“ it builds, is an ensemble of Decision Trees [2], most of the time trained with the “bagging” method. The general idea of the bagging method is that a combination of learning models increases the overall result [1].

## Difference between Decision Trees and Random Forests:
> Another difference is that „deep“ decision trees might suffer from overfitting. Random Forest prevents overfitting most of the time, by creating random subsets of the features and building smaller trees using these subsets. Afterwards, it combines the subtrees. Note that this doesn’t work every time and that it also makes the computation slower, depending on how many trees your random forest builds [1].

## Ensemble Learning


### Algorithm
```sh 
STEP 1: Pick at random K data points from the training set.
STEP 2: Build the Decision Tree associated to there k data points.
STEP 3: Choose the number Ntrees of trees you want to build and repeat STEPS 1 and 2.
STEP 4: For a new data point, make each one of your Ntree trees predict the value of Y to for the data point in question, and assign the new data point the average across all of predicted Y values.
```

Algorithm is took from Machine Learning Course A-Z on Udemy [3].


## Observation

Random Forest is an easy and powerful model. It's a good method when you have missing values or missing data and the method won't overfit the model. Random Forest is able to handle large dataset with high dimensionality.
Besides this  advantages, it's not as good regressor as classificator and you have little control on what model does.

## References

[1] https://towardsdatascience.com/the-random-forest-algorithm-d457d499ffcd

[2] https://gitlab.com/felipeiex/Decision-Tree-Regression

[3] https://www.udemy.com/machinelearning/